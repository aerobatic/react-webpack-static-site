import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import {
  Router,
  RouterContext,
  match,
  browserHistory,
  createMemoryHistory
} from 'react-router';

import template from 'ejs!../index.ejs';   //eslint-disable-line import/no-unresolved
import routes from './routes';

// client render (optional)
if (typeof document !== 'undefined') {
  const outlet = document.getElementById('outlet');
  ReactDOM.render(<Router history={browserHistory} routes={routes} />, outlet);
}

// Exported static site renderer:
export default (locals, callback) => {
  const history = createMemoryHistory();
  const location = history.createLocation(locals.path);
  match({ routes, location }, (error, redirectLocation, renderProps) => {
    const body = ReactDOMServer.renderToString(<RouterContext {...renderProps} />);

    const pageData = {
      title: locals.title,
      body: body
    };
    const markup = template(pageData) //`<div id="outlet2">${body}</div>`;
    callback(null, markup);
  });
};
