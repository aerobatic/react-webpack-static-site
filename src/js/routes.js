import React from 'react'
import App from './app'
import Home from './home'
import About from './about'
import { IndexRoute, Router, Route, browserHistory } from 'react-router'

export default
  (
      <Route path='/' component={App}>
        <IndexRoute component={Home} />
        <Route path='/about' component={About} />
      </Route>
  )
