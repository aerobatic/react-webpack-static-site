import React from 'react'
import { Link } from 'react-router'

const App = (props) => {
  return (
    <div>
      <h1>App</h1>
      <ul role="nav">
        <li><Link activeClassName="active" to="/">Home</Link></li>
        <li><Link activeClassName="active" to="/about">About</Link></li>
      </ul>
      {props.children}
    </div>
  )
}

export default App
