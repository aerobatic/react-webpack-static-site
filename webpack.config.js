/* eslint-disable no-var */
/* eslint-disable immutable/no-mutation */

var StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var locals = {
  title: 'My static site',
  // put the paths of your site here.
  paths: [
    '/',
    '/about/'
  ]
};

module.exports = {
  devtool: 'source-map',
  devServer: {
    contentBase: 'build',
    port: process.env.PORT || 3000,
    host: process.env.HOST || '0.0.0.0',
    colors: true,
    progress: true,
    historyApiFallback: true
  },
  context: __dirname + '/src',
  entry: {
    main: './js/index.js'
  },
  output: {
    filename: 'bundle.js',
    path: __dirname + '/build',
    libraryTarget: 'umd'
  },

  plugins: [
    new StaticSiteGeneratorPlugin( 'bundle.js', locals.paths, locals ),
    new webpack.NoErrorsPlugin()
  ],

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2016', 'react']
        }
      },
      { test: /\.css$/, loader: 'style!css' }
    ]
  }
};
